





var NS = NS || {};
window.addEventListener("load", function() {

	function isLocalStorage(){
		var test = 'test';
		try {
			localStorage.setItem(test, test);
			localStorage.removeItem(test);
			return true;
		} catch(e) {
			return false;
		}
	}

	var loader = NS.sessionStatusLoader;
	var ui = NS.sessionStatusUI;
	var bus;
	if (isLocalStorage() === true) {
		bus = NS.sessionStatusBus;
	}
	else {
		bus = NS.sessionStatusBusLegacy;
	}

	loader.init.bind(loader.internal) ({
		"successListener": bus.sessionStatusLoadSuccess.bind(bus.internal),
		"errorListener": bus.sessionStatusLoadError.bind(bus.internal)
	});

    

	ui.init.bind(ui) ({
        "id": 231230141,
        "entityId": 46082,
		"roleId": 1012,
		"companyId": "3857618",
		"refreshHandler": function() { bus.schedule.bind(bus.internal)(0); /* immediately*/},
        translation: {
            loggedOut: {
                title: "Sie wurden abgemeldet.",
                description: "Klicken Sie auf die Schaltfläche, um sich erneut anzumelden.",
                button: "Anmeldung"
            },
            wrongRole: {
                title: "Für dieses Tab ist eine andere Rolle erforderlich.",
                description: "Bitte wechseln Sie zur Rolle <b>HISS-TEC Sales Support und Versand</b> in  <b>Hiss-Tec GmbH</b>, indem Sie auf die folgende Schaltfläche klicken.",
                button: "Rolle ändern"
            },
            expiring: {
                title: "Ihre Sitzung läuft ab.",
                description: "Sie waren für einige Zeit inaktiv. Ihre Arbeitssitzung läuft in <b id='count'>?</b> Sekunde(n) ab.",
                button: "Sitzung aktiv halten"
            },
            connectionError: {
                title: "Offline"
            }
        },
    });

	bus.init.bind(bus.internal) ({
		"sessionStatusLoader": loader.load.bind(loader.internal),
		"sessionStatusListener" : ui.onSessionStatus.bind(ui),
		"errorListener": ui.onError.bind(ui)
	});

	// starting the bus
	if (null=== true) {
		bus.schedule.bind(bus.internal)(0);
	}
	else {
		bus.schedule.bind(bus.internal)(bus.internal.normalInterval);
	}

}, false);
