// FedexMail.js  version 0.1
// Author: Sven Schiemann
// This script creates a Mail with some important informations for a FedEx registration


//jQuery Start:

!function (sms) {
    sms(document).ready(function () {

        function sendMail(){
          // Register Vars
          var orderNr,
              adress,
              phone,
              fedexEmail = 'fedex@carved.de',
              hint,
              shipping,
              subject,
              emailBody;

          // Save TextContent form Netsuite in Vars for FedEx Mail
          oderNr = sms('.uir-record-id').text();
          // get shipping from DOM, replace irrelevant informations
          shipping = sms('#shipmethod_fs_lbl_uir_label').parent().text().replace('Versandmethode', "").trim();
          // get get phone nr from DOM, add it to var adress
          phone = sms('*[data-field-type="phone"]').first().text();
          // get Adress from DOM, remove word 'Adress', add it to var adress, format text, remove a-tags, remove &nbsp entity;
          adress = sms('*[data-field-type="address"] .uir-field').first().html().replace(/<br.*?>/g, '%0A').replace(/<a.*?<\/a>/g, "").replace(/&nbsp;/,' ').trim();
          // get hint from DOM
          hint = sms('#custbody_hintpacking_fs_lbl').parent().parent().text();

          // create vars for the mailto function
          emailBody = '###########%0A' + hint +'%0A##########%0A%0A'+ adress + '%0A%0A' + phone;
          subject = shipping + ' – '+ oderNr;
          window.location = 'mailto:' + fedexEmail + '?subject=' + subject + '&body='+ emailBody ;
          }

          // button to trigger the function
          sms('#sendMail').click(function( event, a, b ) {
            sendMail();
          });
});




}(jQuery);
// TODO:
// bind function on a hotkey like "ctl+m"
// https://github.com/jeresig/jquery.hotkeys
// TODO: Input Dialog
/*
function Box() {
    var Boxes = [];

    for (var i = 0; i < coords.length; ++i) {
        Boxes[i] =  prompt("Enter the Box IF.", "Box Nummer x");
    }
    if (Boxes != null) {
      //  document.getElementById("demo").innerHTML =
    }
};
*/
// TODO: format Email body with regEx
// catch  carriage returns : \r
//var emailBody = htmlBody.replace(/<br\s*\/?>/mg,"%0D%0A");
