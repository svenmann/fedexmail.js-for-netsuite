


















var BGBUTTON = "3B89D8";

var isNetscape = true;
var isFirefox = false;
var isChrome = true;
var isSafari = true;
var isIPHONE = false;
var isSAFARI = true;
var isMAC = true;
var isSuitePhone = false;

var isCustomerFacingEnvironment = true;

// Do this once to make sure the zoom factor is set for the default value, in case it isn't called.
var ZOOM_FACTOR = 1.0;
var ADMI_HEADING = "ADMI_HEADING";


var portletMaxControlImgUrl = "/images/icons/dashboard/portletelements/chiles/maximize.gif";
var portletMaxControlHighLightImgUrl = "/images/icons/dashboard/portletelements/chiles/maximize_hl.gif";
var portletMinControlImgUrl = "/images/icons/dashboard/portletelements/chiles/minimize.gif";
var portletMinControlHighLightImgUrl = "/images/icons/dashboard/portletelements/chiles/minimize_hl.gif";

var NLField_MANDATORY = 128;
var NLField_DEFAULT = 0;
var NLFieldConstants_DISABLED = 2048;
var NLFieldConstants_TYPEAHEAD = 2147483648;
var NLFieldConstants_NOARROW = 1073741824;
var NLFieldConstants_EXTREME = 256;

// -- initialize editor fonts
var fontNames =	{
    "Schriftart" : "",
					"Verdana" : "Verdana",
					"Arial"	: "Arial",
					"Courier New" : "Courier New",
					"Times New Roman" : "Times New Roman",
					"Comic Sans" : "Comic Sans MS",
					"Georgia" : "Georgia",
					"Tahoma" : "Tahoma",
					"Trebuchet" : "Trebuchet MS"
				};

// -- initialize editor font colors
var fontColors = {
    "Farbe"          : "",
    "Schwarz"         : "#000000",
    "Rot"	        : "#FF0000",
    "Blau"          : "#0000FF",
    "Dunkelblau"     : "#00008B",
    "Marineblau"     : "#000080",
    "Braun"         : "#A52A2A",
    "Grün"         : "#008000",
    "Orange"        : "#FFA500",
    "Hellgrau"    : "#D3D3D3",
    "Silber"        : "#C0C0C0"
};

// -- initialize editor font sizes
var fontSizes =	{
    "Größe" : "",
    "8"  :  "1",
    "10" : "2",
    "12" : "3",
    "14" : "4",
    "18" : "5",
    "24" : "6",
    "36" : "7"
};

// translated RTE labels and helper text
var NLHTMLEDITOR_FORMATTED_LABEL = "Formatierter Text";
var NLHTMLEDITOR_SOURCECODE_LABEL = "HTML-Quellenkode";
var NLHTMLEDITOR_FORMATTED_HELPER_TEXT = "Schreiben Sie Text und formatieren Sie ihn mithilfe der Instrumentensymbolleiste.";
var NLHTMLEDITOR_FORMATTED_HELPER_TEXT_WITH_STYLE = "<font color=\"#666666\">"+NLHTMLEDITOR_FORMATTED_HELPER_TEXT+"</font>";
var NLHTMLEDITOR_SOURCECODE_HELPER_TEXT = "<!-- HTML-Code schreiben oder einfügen -->";
var NLHTMLEDITOR_STYLE_SMALL_TEXT = "smalltext";

var iMAX_SUGGESTIONS = 25;

var NLHEADING_NO_RESULTS_FOUND = "Keine Ergebnisse gefunden";
var PAGE_LOADING = "Ladevorgang";
var PAGE_NO_SELECTIONS_MADE = "Keine Auswahl getroffen";
var HEADING_MORE_OPTIONS = "Weitere Optionen";
var PAGE_REFRESHING = "Aktualisierung";
var HEADING_PLEASE_ENTER_MORE_CHARACTERS_OR_CLICK_GO = "Geben Sie weitere Zeichen ein und klicken Sie auf „Start”.";
var BUTTON_VIEW_DASHBOARD = "Ansicht Dashboard";
var PAGE_EDIT = "Bearbeiten";
var PAGE_MORE = "Mehr...";

// Help strings for NLPopupSelect widgets
var _popup_help = '<Geben Sie die ersten Buchstaben ein, gefolgt von Tab>';
var _short_popup_help = '<„Text eingeben, dann Tab-Taste drücken”>';
var _mult_popup_help = '<Eingabe und Tab für einzelnen Wert>';
var _popup_help_search = 'Eingabe und Tabulator...';

var FIELD_DATA = "_mdata";
var FIELD_LABEL = "_mlabels";

function NLHtmlEditor_buildToolBar()
{
	this.toolbar = document.createElement("DIV");
	this.toolbar.id = this.name+'_toolbar';
	this.toolbar.style.padding = '4px 4px 2px 4px';
	this.toolbar.style.height = '22px';
	this.toolbar.unselectable = 'on';
	this.toolbar.style.backgroundColor = "#ECEFF6";
	this.toolbar.style.borderWidth='0px';
	this.main.appendChild( this.toolbar );

	// -- now add toolbar icons
	this.buildToolBarIcon('fontname', 'Schriftart', 'FontName');
	this.buildToolBarIcon('fontsize', 'Größe', 'FontSize');
	this.buildToolBarIcon('fontcolor', 'Farbe', 'ForeColor');
	if ( isNaN(this.width) || parseInt(this.width) > 425 )
		this.buildToolBarLiner();
	else
		this.buildToolBarLineBreak();
	this.buildToolBarIcon('bold', 'Fettdruck', 'Bold');
	this.buildToolBarIcon('italic', 'Kursiv', 'Italic');
	this.buildToolBarIcon('underline', 'Unterstreichen', 'Underline');
	this.buildToolBarLiner();
	this.buildToolBarIcon('justifyleft', 'Linksbündig', 'JustifyLeft');
	this.buildToolBarIcon('justifycenter', 'Zentrieren', 'JustifyCenter');
	this.buildToolBarIcon('justifyright', 'Rechtsbündig', 'JustifyRight');
    if (!isSafari) { // the 4 commands below are not supported by safari 2%>
    this.buildToolBarLiner();
	this.buildToolBarIcon('insertorderedlist', 'Liste\x20Bestellt', 'InsertOrderedList');
	this.buildToolBarIcon('insertunorderedlist', 'Nicht\x20sortierte\x20Liste', 'InsertUnorderedList');
	this.buildToolBarIcon('outdent', 'Hängender Einzug', 'Outdent');
	this.buildToolBarIcon('indent', 'Auftrag', 'Indent');
   }
}
