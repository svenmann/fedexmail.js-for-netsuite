









window.NS = window.NS || {};
window.NS.Workflow = window.NS.Workflow || {};

window.NS.Workflow.getButtons = function() {
	var buttons = [];
	var buttonTokens = nlapiGetFieldValue('workflowbuttons').split('\u0005');
	for (var i = 0; i * 4 < buttonTokens.length; i++) {
		var button = {
			name: buttonTokens[i * 4],
			label: buttonTokens[i * 4 + 1],
			actionId: buttonTokens[i * 4 + 2],
			workflowInstanceId: buttonTokens[i * 4 + 3]};
		buttons[i] = buttons[button.name] = button;
	}
	return buttons;
};

window.NS.Workflow.buttonClick = function(clickedButton) {
	var location = document.location.href;
	var buttons = window.NS.Workflow.getButtons();

	var previousButton = nlapiGetFieldValue('workflowbuttonclicked');
	if (previousButton) {
		var message = clickedButton == previousButton
				? 'Sie haben bereits die Schaltfläche \"{1:current button}\" betätigt. Die Aktion wird bearbeitet, möchten Sie mit der Bearbeitung der Aktion erneut beginnen?'
				: 'Sie haben die Schaltfläche \"{1:current button}\" betätigt, die Teil der gleichen Schaltflächengruppe wie die zuvor gedrückte Schaltfläche „{2: previous button}“ ist. Die zuvor begonnene Verarbeitung ist noch im Verlauf. Möchten Sie eine weitere beginnen?';
		if (!confirm(format_message(message, buttons[clickedButton].label, buttons[previousButton].label)))
			return;
		location = addParamToURL(location, 'workflowbuttonmultipleclick', 'T');
	}
	nlapiSetFieldValue('workflowbuttonclicked', clickedButton);
	location = addParamToURL(location, 'workflowbutton', buttons[clickedButton].actionId);
	location = addParamToURL(location, 'workflowbuttoninstanceid', buttons[clickedButton].workflowInstanceId);
	document.location = location;
};